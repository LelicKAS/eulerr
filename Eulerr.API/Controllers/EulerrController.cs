﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eulerr.API.Domain.Requests;
using Eulerr.API.Domain.Responses;
using Eulerr.API.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace Eulerr.API.Controllers
{
    #region EulerrController
    [Route("api/[controller]")]
    public class EulerrController : Controller
    {
        private IEulerrService _eulerrService = null;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="eulerrService">Eulerr Service</param>
        public EulerrController(IEulerrService eulerrService)
        {
            _eulerrService = eulerrService;
        }

        /// <summary>
        /// Endpoint for checking service availability
        /// </summary>
        /// <returns>PONG</returns>
        [HttpGet]
        public IActionResult Get()
        {
            var result = "PONG";
            return Ok(result);
        }

        /// <summary>
        /// Endpoint for optimization Circles data
        /// </summary>
        /// <param name="request">Data for optimization</param>
        /// <returns>Optimized data</returns>
        [HttpPost]
        [Route("optimize")]
        public async Task<OptimizeResponse> Optimize([FromBody] OptimizeRequest request)
        {
            var response = await _eulerrService.Optimize(request);
            return response;
        }

        /// <summary>
        /// Endpoint for calculation Circles data
        /// </summary>
        /// <param name="request">Calculation data</param>
        /// <returns>Optimized data</returns>
        [HttpPost]
        [Route("calc")]
        public async Task<OptimizeResponse> Calc([FromBody] CalcRequest request)
        {
            var response = await _eulerrService.Calc(request);
            return response;
        }
    }
    #endregion
}
