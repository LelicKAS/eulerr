﻿using System;
using System.Text.Json.Serialization;

namespace Eulerr.API.Domain.Responses
{
    #region OptimizeResponse
    /// <summary>
    /// Response Data Model - Result optimization
    /// </summary>
    public class OptimizeResponse
    {
        /// <summary>
        /// Optimized circle data array
        /// </summary>
        [JsonPropertyName("parameters")]
        public double[] Parameters { get; set; }
    }
    #endregion
}
