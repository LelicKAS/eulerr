﻿using System;
using System.Threading.Tasks;
using Eulerr.API.Domain.Requests;
using Eulerr.API.Domain.Responses;

namespace Eulerr.API.Domain.Services
{
    #region IEulerrService
    public interface IEulerrService
    {
        Task<OptimizeResponse> Optimize(OptimizeRequest request);

        Task<OptimizeResponse> Calc(CalcRequest request);
    }
    #endregion
}
