﻿using System;

namespace Eulerr.API.Domain.Classes.Eulerr
{
    #region Ellipse
    internal class Ellipse : IComparable<Ellipse>
    {
        public Ellipse()
        {
        }

        public Ellipse(double size)
        {
            var radius = Math.Sqrt(size / Math.PI);
            this.A = radius;
            this.B = radius;
            this.Size = size;
        }

        public Ellipse(double x, double y, double a, double b, double angle)
        {
            this.X = x;
            this.Y = y;
            this.A = a;
            this.B = b;
            this.Angle = angle;
        }

        public double Area()
        {
            return this.A * this.B * Math.PI;
        }

        public double Sector(double angle)
        {
            return 0.5d * this.A * this.B *
                (angle - Math.Atan2((this.B - this.A) * Math.Sin(2.0d * angle), this.B + this.A + (this.B - this.A) * Math.Cos(2.0d * angle)));
        }

        #region IComporable <Ellipse>
        public int CompareTo(Ellipse other)
        {
            return this.Area().CompareTo(other.Area());
        }
        #endregion

        public double X { get; set; } = 0.0d;

        public double Y { get; set; } = 0.0d;

        public double A { get; set; } = 0.0d;

        public double B { get; set; } = 0.0d;

        public double Angle { get; set; } = 0.0d;

        public double Size { get; set; } = 0.0d;
    }
    #endregion
}
