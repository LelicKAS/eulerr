﻿using System;
namespace Eulerr.API.Domain.Classes.Eulerr
{
    #region Intersection
    internal class Intersection
    {
        public Intersection(Ellipse firstCircle, Ellipse secondCircle, double size)
        {
            this.FirstCircle = firstCircle;
            this.SecondCircle = secondCircle;
            this.Size = size;

            this.CalcDistance();
        }

        private void CalcDistance()
        {
            var r1 = this.FirstCircle.A;
            var r2 = this.SecondCircle.A;
            var distance = r1 + r2 + 0.1;
            var firstAngle = 0d;
            var secondAngle = 0d;
            if ((this.Size <= this.FirstCircle.Size) &&
                (this.Size >= this.SecondCircle.Size))
            {
                var deltaR = Math.Abs(r1 - r2);
                distance = deltaR - (deltaR / 2.0 > 0.2 ? 0.2 : deltaR);
            }
            else if (this.Size > 0)
            {
                var d = r1 + r2 - 0.001;
                var delta = 0d;
                while (d > Math.Abs(r1 - r2))
                {
                    var f1 = 2 * Math.Acos((r1 * r1 - r2 * r2 + d * d) / (2 * r1 * d));
                    var f2 = 2 * Math.Acos((r2 * r2 - r1 * r1 + d * d) / (2 * r2 * d));
                    var s1 = (r1 * r1 * (f1 - Math.Sin(f1))) / 2;
                    var s2 = (r2 * r2 * (f2 - Math.Sin(f2))) / 2;
                    var s = s1 + s2;
                    if (s < this.Size)
                    {
                        delta = this.Size - s;
                        distance = d;
                        firstAngle = f1;
                        secondAngle = f2;
                        d -= 0.001;
                    }
                    else
                    {
                        if (Math.Abs(this.Size - s) < delta)
                        {
                            distance = d;
                            firstAngle = f1;
                            secondAngle = f2;
                        }
                        d = Math.Abs(r1 - r2);
                    }

                }
            }

            this.Distanсe = distance;
            this.FirstAngle = firstAngle;
            this.SecondAngle = secondAngle;
        }

        public Ellipse FirstCircle { get; private set; }

        public Ellipse SecondCircle { get; private set; }

        public double Size { get; private set; }

        public double Distanсe { get; set; }

        public double FirstAngle { get; private set; }

        public double SecondAngle { get; private set; }
    }
    #endregion
}
