﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace Eulerr.API.Domain.Classes.Eulerr.Calculators
{
    #region Calculator<T>
    internal class Calculator<T> : ICalculator<T>
    {
        #region Static methods
        private static readonly Dictionary<RuntimeTypeHandle, IGenericCalculator> __calculators = new Dictionary<RuntimeTypeHandle, IGenericCalculator>();

        static Calculator()
        {
            AddMatrixCalculator<double>(new DoubleCalculator());
            AddMatrixCalculator<int>(new IntCalculator());
            AddMatrixCalculator<Complex>(new ComplexCalculator());
        }

        public static void AddMatrixCalculator<TT>(ICalculator<TT> calculator)
        {
            __calculators.Add(typeof(TT).TypeHandle, calculator);
        }

        public static ICalculator<TT> GetCalculator<TT>()
        {
            IGenericCalculator calculator = null;
            __calculators.TryGetValue(typeof(TT).TypeHandle, out calculator);
            if (calculator == null)
                calculator = new Calculator<TT>();
            return (ICalculator<TT>)calculator;
        }

        public static TT ThrowException<TT>()
        {
            throw new NotImplementedException($"Метод не поддерживает тип {typeof(TT)}");
        }
        #endregion

        #region IMatrixCalculator<T>
        public T Addition(T a, T b) => ThrowException<T>();

        public T Divide(T a, T b) => ThrowException<T>();

        public T Multiply(T a, T b) => ThrowException<T>();

        public T Subtract(T a, T b) => ThrowException<T>();

        public T Pow(T a, T b) => ThrowException<T>();

        public T Abs(T a) => ThrowException<T>();

        public int Compare(T a, T b)
        {
            ThrowException<T>();
            return 0;
        }

        public bool IsFinite(T a)
        {
            ThrowException<T>();
            return false;
        }

        public T Det(Matrix<T> matrix) => ThrowException<T>();
        #endregion
    }
    #endregion
}
