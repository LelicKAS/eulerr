﻿using System;
namespace Eulerr.API.Domain.Classes.Eulerr.Calculators
{
    #region DoubleCalculator
    internal class DoubleCalculator : ICalculator<double>
    {
        #region IMatrixCalculator<double>
        public double Addition(double a, double b) => a + b;

        public double Subtract(double a, double b) => a - b;

        public double Multiply(double a, double b) => a * b;

        public double Divide(double a, double b) => a / b;

        public double Pow(double a, double b) => Math.Pow(a, b);

        public double Abs(double a) => Math.Abs(a);

        public int Compare(double a, double b) => a.CompareTo(b);

        public bool IsFinite(double a) => double.IsFinite(a);

        public double Det(Matrix<double> matrix)
        {
            var result = matrix.DefaultValue;
            if (matrix.Cols != matrix.Rows)
                throw new Exception("Determinant not found!");
            if (matrix.Cols == 1)
                result = matrix[0, 0];
            else
            {
                for (var j = 0; j < matrix.Cols; j++)
                {
                    var subMatrix = matrix.SubMatrix(0, j);
                    result += this.Pow(-1.0d, j + 2.0d) * matrix[0, j] * this.Det(subMatrix);
                }
            }
            return result;
        }
        #endregion
    }
    #endregion
}
