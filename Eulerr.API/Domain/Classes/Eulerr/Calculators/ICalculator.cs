﻿using System;
namespace Eulerr.API.Domain.Classes.Eulerr.Calculators
{
    #region IGenericCalculator
    interface IGenericCalculator
    {
    }
    #endregion

    #region ICalculator<T>
    interface ICalculator<T> : IGenericCalculator
    {
        T Addition(T a, T b);

        T Subtract(T a, T b);

        T Multiply(T a, T b);

        T Divide(T a, T b);

        T Pow(T a, T b);

        T Abs(T a);

        int Compare(T a, T b);

        bool IsFinite(T a);

        T Det(Matrix<T> matrix);
    }
    #endregion
}
