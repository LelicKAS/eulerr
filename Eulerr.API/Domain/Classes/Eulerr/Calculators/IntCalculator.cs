﻿using System;
namespace Eulerr.API.Domain.Classes.Eulerr.Calculators
{
    #region IntMatrixCalculator
    internal class IntCalculator : ICalculator<int>
    {
        #region ICalculator<int>
        public int Addition(int a, int b) => a + b;

        public int Subtract(int a, int b) => a - b;

        public int Multiply(int a, int b) => a * b;

        public int Divide(int a, int b) => a / b;

        public int Pow(int a, int b) => (int)Math.Pow(a, b);

        public int Abs(int a) => Math.Abs(a);

        public int Compare(int a, int b) => a.CompareTo(b);

        public bool IsFinite(int a) => true;

        public int Det(Matrix<int> matrix)
        {
            var result = matrix.DefaultValue;
            if (matrix.Cols != matrix.Rows)
                throw new Exception("Determinant not found!");
            if (matrix.Cols == 1)
                result = matrix[0, 0];
            else
            {
                for (var j = 0; j < matrix.Cols; j++)
                {
                    var subMatrix = matrix.SubMatrix(0, j);
                    result += this.Pow(-1, j + 2) * matrix[0, j] * this.Det(subMatrix);
                }
            }
            return result;
        }
        #endregion
    }
    #endregion
}
