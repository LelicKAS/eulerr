﻿using System;
using System.Numerics;

namespace Eulerr.API.Domain.Classes.Eulerr.Calculators
{
    #region ComplexMatrixCalculator
    internal class ComplexCalculator : ICalculator<Complex>
    {
        #region IMatrixCalculator<Complex>
        public Complex Addition(Complex a, Complex b) => a + b;

        public Complex Subtract(Complex a, Complex b) => a - b;

        public Complex Multiply(Complex a, Complex b) => a * b;

        public Complex Divide(Complex a, Complex b) => a / b;

        public Complex Pow(Complex a, Complex b) => Calculator<Complex>.ThrowException<Complex>();

        public int Compare(Complex a, Complex b) => a.Real.CompareTo(b.Real);

        public bool IsFinite(Complex a) => double.IsFinite(a.Real);

        public Complex Det(Matrix<Complex> matrix) => Calculator<Complex>.ThrowException<Complex>();

        public Complex Abs(Complex a) => Calculator<Complex>.ThrowException<Complex>();
        #endregion
    }
    #endregion
}
