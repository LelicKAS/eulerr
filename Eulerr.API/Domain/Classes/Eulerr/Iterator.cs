﻿using System;
using System.Collections.Generic;

namespace Eulerr.API.Domain.Classes.Eulerr
{
    #region Iterator
    internal class Iterator
    {
        public Iterator(int index, double value, double delta, int iterations)
        {
            this.Index = index;

            var values = new List<double>();

            var fromSteps = iterations / 2;
            var step = delta / iterations;

            var fromValue = value - (fromSteps * step);

            for (var i = 0; i < iterations; i++)
            {
                values.Add(fromValue);
                fromValue += step;
            }

            this.Values = values.ToArray();

        }

        public int Index { get; private set; }

        public double[] Values { get; private set; }
    }
    #endregion
}
