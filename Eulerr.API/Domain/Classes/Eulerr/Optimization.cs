﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace Eulerr.API.Domain.Classes.Eulerr
{
    #region Optimization
    public class Optimization
    {
        #region Static member
        public static double[] UnionAreas(double[] areas)
        {
            var unionAreas = new List<double>();
            if (areas.Length == 1)
                unionAreas.Add(areas[0]);
            else if (areas.Length == 3)
            {
                unionAreas.Add(areas[0] + areas[2]);
                unionAreas.Add(areas[1] + areas[2]);
                unionAreas.Add(areas[2]);
            }
            else if (areas.Length == 7)
            {
                unionAreas.Add(areas[0] + areas[3] + areas[4] + areas[6]);
                unionAreas.Add(areas[1] + areas[3] + areas[5] + areas[6]);
                unionAreas.Add(areas[2] + areas[4] + areas[5] + areas[6]);
                unionAreas.Add(areas[3] + areas[6]);
                unionAreas.Add(areas[4] + areas[6]);
                unionAreas.Add(areas[5] + areas[6]);
                unionAreas.Add(areas[6]);
            }
            return unionAreas.ToArray();
        }

        public static double[] DisjointAreas(double[] areas)
        {
            var disjointAreas = new List<double>();
            if (areas.Length == 1)
                disjointAreas.Add(areas[0]);
            else if (areas.Length == 3)
            {
                disjointAreas.Add(areas[0] - areas[2]);
                disjointAreas.Add(areas[1] - areas[2]);
                disjointAreas.Add(areas[2]);
            }
            else if (areas.Length == 7)
            {
                disjointAreas.AddRange(areas);
                disjointAreas[5] = areas[5] - areas[6];
                disjointAreas[4] = areas[4] - areas[6];
                disjointAreas[3] = areas[3] - areas[6];
                disjointAreas[2] = areas[2] - (areas[4] - areas[6]) - (areas[5] - areas[6]) - areas[6];
                disjointAreas[1] = areas[1] - (areas[3] - areas[6]) - (areas[5] - areas[6]) - areas[6];
                disjointAreas[0] = areas[0] - (areas[3] - areas[6]) - (areas[4] - areas[6]) - areas[6];
            }
            return disjointAreas.ToArray();
        }
        #endregion

        public Optimization(double[] sourceParameters, double[] sourceAreas)
        {
            this.SourceParameters = sourceParameters.ToArray();
            this.SourceAreas = sourceAreas.ToArray();
            this.TargetParameters = sourceParameters.ToArray();
        }

        public override string ToString()
        {
            var sourceStrings = this.SourceParameters
                .Select(v => v.ToString())
                .ToArray();
            var targetStrings = this.TargetParameters
                .Select(v => v.ToString())
                .ToArray();
            var toString = Environment.NewLine +
                "Source: { " + String.Join(", ", sourceStrings) + " }" + Environment.NewLine +
                "Target: { " + String.Join(", ", targetStrings) + " }" + Environment.NewLine +
                $"Duration: {this.DurationTime}";
            return toString;
        }

        public async Task Optimize()
        {
            await Task.Run(() => {
                var startTime = DateTime.Now;

                var pars = new Vector<double>(this.SourceParameters);

                var areas = new Vector<double>(UnionAreas(this.SourceAreas));

                var point = new Point(pars[6], pars[7]);
                var centerPoint = new Point(pars[3], pars[4]);

                var minResult = Double.MaxValue;
                var minPars = pars.Clone();

                var angle = 0.0;
                var delta = Math.PI / 360.0d;
                while (angle < Math.PI * 2.0d)
                {
                    var checkPoint = point.Clone();
                    checkPoint.Rotate(angle, centerPoint);
                    pars[6] = checkPoint.X;
                    pars[7] = checkPoint.Y;
                    var fit = this.IntersectEllipses(pars, true);
                    var result = fit.InnerProduct(areas, (a, b) => (a - b) * (a - b));

                    Console.WriteLine($"{result}, {minResult}, {this.CheckFit(areas, fit)}");

                    var disjointFit = new Vector<double>(DisjointAreas(fit.ToArray()));

                    if ((result < minResult) && (this.CheckFit(areas, disjointFit)))
                    {
                        Console.WriteLine("<=== Minimum");
                        minResult = result;
                        minPars = pars.Clone();

                        //if (angle == 0)
                        //    angle = Math.PI * 2.0d;
                    }

                    angle += delta;
                }

                this.TargetParameters[6] = minPars[6];
                this.TargetParameters[7] = minPars[7];

                var endTime = DateTime.Now;
                this.DurationTime = endTime - startTime;
            });
        }

        private bool CheckFit(Vector<double> areas, Vector<double> fit)
        {
            var result = true;
            var index = 0;
            while ((index < areas.Size) && (result))
            {
                result = (((areas[index] > 0) && (fit[index] > 0)) || ((areas[index] == 0) && (fit[index] == 0)));
                index++;
            }
            return result;
        }

        private bool NearlyEquals(double a, double b)
        {
            return (Math.Abs(a - b) <= Double.Epsilon * Math.Max(Math.Abs(a), Math.Abs(b)));
        }

        private Vector<Complex> SolveCubic(double alpha, double beta, double gamma, double delta)
        {
            var y = new Vector<Complex>(3);

            var i = new Complex(0.0d, 1.0d);

            var a = beta / alpha;
            var b = gamma / alpha;
            var c = delta / alpha;
            var Q = (a * a - 3.0d * b) / 9.0d;
            var R = (2.0 * a * a * a - 9.0d * a * b + 27.0d * c) / 54.0d;

            if (R * R < Q * Q * Q)
            {
                var theta = Math.Acos(R / Math.Sqrt(Q * Q * Q));
                y[0] = -2.0d * Math.Sqrt(Q) * Math.Cos(theta / 3.0d) - a / 3.0;
                y[1] = -2.0d * Math.Sqrt(Q) * Math.Cos((theta + 2.0d * Math.PI) / 3.0d) - a / 3.0d;
                y[2] = -2.0d * Math.Sqrt(Q) * Math.Cos((theta - 2.0d * Math.PI) / 3.0d) - a / 3.0d;
            }
            else
            {
                var A = -Math.Sign(R) * Math.Cbrt(Math.Abs(R) + Math.Sqrt(R * R - Q * Q * Q));
                var B = this.NearlyEquals(A, 0.0d) ? 0.0d : Q / A;
                y[0] = A + B - a / 3.0d;
                y[1] = -0.5 * (A + B) - a / 3.0d + Math.Sqrt(3.0d) * i * (A - B) / 2.0d;
                y[2] = -0.5 * (A + B) - a / 3.0d - Math.Sqrt(3.0d) * i * (A - B) / 2.0d;
            }

            return y;
        }

        private Matrix<double> Adjoint(Matrix<double> m)
        {
            var result = new Matrix<double>(3, 3);

            var a = m[0, 0];
            var b = m[1, 0];
            var c = m[1, 1];
            var d = m[2, 0];
            var e = m[2, 1];
            var f = m[2, 2];

            result[0, 0] = c * f - e * e;
            result[1, 0] = d * e - b * f;
            result[1, 1] = a * f - d * d;
            result[2, 0] = b * e - c * d;
            result[2, 1] = b * d - a * e;
            result[2, 2] = a * c - b * b;

            return result.SymmatL();
        }

        private Matrix<Complex> Skewsymmat(Vector<Complex> v)
        {
            var result = new Matrix<Complex>(3, 3);
            result.ForEach((m, i, j) =>
            {
                if (i == j)
                    m[i, j] = m.DefaultValue;
            });
            result[0, 1] = v[2];
            result[0, 2] = -v[1];
            result[1, 0] = -v[2];
            result[1, 2] = v[0];
            result[2, 0] = v[1];
            result[2, 1] = -v[0];
            return result;
        }

        private Matrix<double> Skewsymmat(Vector<double> v)
        {
            var result = new Matrix<double>(3, 3);
            result.ForEach((m, i, j) =>
            {
                if (i == j)
                    m[i, j] = m.DefaultValue;
            });
            result[0, 1] = v[2];
            result[0, 2] = -v[1];
            result[1, 0] = -v[2];
            result[1, 2] = v[0];
            result[2, 0] = v[1];
            result[2, 1] = -v[0];
            return result;
        }

        private Matrix<double> SplitConic(Matrix<double> A)
        {
            var B = Adjoint(A).Multiply(-1);

            var i = B.Diagonal().MaxIndex();

            var Bii = new Complex();
            Bii = Math.Sqrt(B[i, i]);

            var result = new Matrix<double>(3, 2);

            if (Bii.Real > 0.0)
            {
                var C = Matrix<double>.DoubleToComplex(A)
                    .Addition(this.Skewsymmat(Vector<double>.DoubleToComplex(B.ColumnByIndex(i)).Divide(Bii)));

                var ij = C.MaxIndex();

                var CRow = C.RowByIndex(ij[0]);
                var CCol = C.ColumnByIndex(ij[1]);

                result.ForEach((m, i, j) => {
                    if (j == 0)
                        m[i, j] = CRow[i].Real;
                    if (j == 1)
                        m[i, j] = CCol[i].Real;
                });
            }

            return result;
        }

        private bool PointInEllipse(Point p, Ellipse e)
        {
            var a = Math.Pow((p.X - e.X) * Math.Cos(e.Angle) + (p.Y - e.Y) * Math.Sin(e.Angle), 2) / (e.A * e.A);
            var b = Math.Pow((p.X - e.X) * Math.Sin(e.Angle) - (p.Y - e.Y) * Math.Cos(e.Angle), 2) / (e.B * e.B);
            var result = a + b;
            return result <= 1.0d;
        }

        private Vector<int> Adopt(Point p, Vector<Ellipse> ellipses, int a, int b)
        {
            var vector = new Vector<int>();
            for (var index = 0; index < ellipses.Size; index++)
                if ((index == a) || (index == b) || this.PointInEllipse(p, ellipses[index]))
                    vector.Add(index);
            return vector;
        }

        private bool IsSubset<T>(Vector<T> a, Vector<T> b) where T : IComparable<T>
        {
            var result = true;
            foreach (var elA in a)
                if (b.IndexOf(elA) < 0)
                {
                    result = false;
                    break;
                }
            return result;
        }

        private double DisjointOrSubset(Vector<Ellipse> ellipses, Vector<int> ind)
        {
            var areas = new Vector<double>();

            foreach (var i in ind)
                areas.Add(ellipses[i].Area());

            var minItr = areas.Min();
            var minInd = ind[areas.IndexOf(minItr)];

            var p = new Point(ellipses[minInd].X, ellipses[minInd].Y);

            var subset = false;

            for (var index = 0; index < ind.Size; index++)
                if (index != minInd)
                {
                    subset = PointInEllipse(p, ellipses[index]);

                    if (!subset)
                        break;
                }

            return subset ? minItr : 0.0d;
        }

        private Vector<int> Seq(int n)
        {
            var result = new Vector<int>(n);
            result.Fill(i => i);
            return result;
        }

        private double EllipseSegment(Ellipse e, Point fp, Point sp)
        {
            var p0 = new Point(fp.X, fp.Y);
            p0.Translate(-e.X, -e.Y);
            p0.Rotate(-e.Angle);
            var p1 = new Point(sp.X, sp.Y);
            p1.Translate(-e.X, -e.Y);
            p1.Rotate(-e.Angle);

            var theta0 = Math.Atan2(p0.Y, p0.X);
            var theta1 = Math.Atan2(p1.Y, p1.X);

            if (theta1 < theta0)
                theta1 += 2.0 * Math.PI;

            var triangle = 0.5 * Math.Abs(p1.X * p0.Y - p0.X * p1.Y);

            return (theta1 - theta0 <= Math.PI
                     ? e.Sector(theta1) - e.Sector(theta0) - triangle
                     : e.Area() - e.Sector(theta0 + 2.0 * Math.PI) + e.Sector(theta1) + triangle);
        }

        private double Polysegments(
                Vector<Point> points,
                Vector<Ellipse> ellipses,
                Vector<Vector<int>> parents,
                Vector<int> intPoints,
                ref bool failure)
        {
            var n = intPoints.Size;

            var h0 = 0.0d;
            var k0 = 0.0d;

            foreach (var i in intPoints)
            {
                h0 += points[i].X / n;
                k0 += points[i].Y / n;
            }

            var angle = new Vector<double>();

            foreach (var i in intPoints)
                angle.Add(Math.Atan2(points[i].X - h0, points[i].Y - k0));

            var ind = this.Seq(n);
            ind.Sort(delegate (int i, int j)
            {
                return angle[i].CompareTo(angle[j]);
            });

            var area = 0.0d;

            var l = n - 1;
            for (var k = 0; k < n; ++k)
            {
                var i = intPoints[ind[k]];
                var j = intPoints[ind[l]];

                var ii = Vector<int>.SetIntersection(parents[i], parents[j]);

                if (ii.Size > 0)
                {
                    var areas = new Vector<double>();

                    foreach (var m in ii)
                        areas.Add(this.EllipseSegment(ellipses[m], points[i], points[j]));

                    area += 0.5 * ((points[j].X + points[i].X) * (points[j].Y - points[i].Y)) + areas.Min();

                }
                else
                {
                    failure = true;
                    return 0.0;
                }

                l = k;
            }
            return area;
        }

        private double Montecarlo(Vector<Ellipse> ellipses, Vector<int> indices)
        {
            var n = indices.Size;

            var areas = new Vector<double>();

            var nPoints = 1e4;

            foreach (var ind in indices)
            {
                var nInside = 0;

                var e = ellipses[ind];

                for (var i = 0; i < nPoints; i++)
                {
                    var theta = i * (Math.PI * (3.0d - Math.Sqrt(5.0d)));
                    var r = Math.Sqrt((double)i / (double)nPoints);

                    var p = new Point(r * Math.Cos(theta), r * Math.Sin(theta));

                    p.Scale(e.A, e.B);
                    p.Rotate(e.Angle);
                    p.Translate(e.X, e.Y);

                    var allInside = indices.AllOf(i => ((i == ind) || (this.PointInEllipse(p, ellipses[i]))));

                    if (allInside)
                        nInside++;
                }

                areas.Add(e.Area() * nInside / nPoints);
            }

            return areas.Accumulate() / n;
        }

        private double Clamp(double x, double lo, double hi)
        {
            return (x < lo ? lo : (x > hi ? hi : x));
        }

        private int Clamp(int x, int lo, int hi)
        {
            return (x < lo ? lo : (x > hi ? hi : x));
        }

        private Vector<double> IntersectEllipses(
            Vector<double> parameters,
            bool circle,
            bool approx = false)
        {
            var nPars = circle ? 3 : 5;
            var n = parameters.Size / nPars;
            var nOverlaps = (int)Math.Pow(2, n) - 1;
            var id = this.SetIndex(n);

            var ellipses = new Vector<Ellipse>();
            for (int i = 0; i < n; i++)
            {
                var x = parameters[i * nPars];
                var y = parameters[i * nPars + 1];
                var a = parameters[i * nPars + 2];
                var b = parameters[i * nPars + 2];
                var angle = 0.0d;
                if (!circle)
                {
                    b = parameters[i * nPars + 3];
                    angle = parameters[i * nPars + 4];
                }
                ellipses.Add(new Ellipse(x, y, a, b, angle));
            }

            var conics = new Vector<Conic>();
            for (int i = 0; i < n; i++)
                conics.Add(new Conic(ellipses[i]));

            var points = new Vector<Point>();
            var parents = new Vector<Vector<int>>();
            var adopters = new Vector<Vector<int>>();

            for (var i = 0; i < n - 1; i++)
            {
                for (var j = i + 1; j < n; j++)
                {
                    var p = this.Intersect(conics[i], conics[j]);
                    foreach (var pi in p)
                    {
                        var parent = new Vector<int>(i, j);
                        parents.Add(parent);
                        adopters.Add(this.Adopt(pi, ellipses, i, j));
                        points.Add(pi);
                    }
                }
            }

            var areas = new Vector<double>();
            var intPoints = new Vector<int>();

            foreach (var idI in id)
            {
                if (idI.Size == 1)
                    areas.Add(ellipses[idI[0]].Area());
                else
                {
                    for (var j = 0; j < parents.Size; j++)
                        if ((this.IsSubset(parents[j], idI)) && (this.IsSubset(idI, adopters[j])))
                            intPoints.Add(j);

                    if (intPoints.Size == 0)
                        areas.Add(this.DisjointOrSubset(ellipses, idI));
                    else
                    {
                        var failure = false;
                        var area = this.Polysegments(points, ellipses, parents, intPoints, ref failure);

                        if (failure || approx)
                            area = this.Montecarlo(ellipses, idI);

                        areas.Add(area);
                    }
                }

                intPoints.Clear();
            }

            var result = (areas.Clone() as Vector<double>);

            result.Transform(a => this.Clamp(a, 0.0d, Double.PositiveInfinity));

            return result;
        }

        private void IntersectConicLine(Matrix<double> A, Vector<double> l, Vector<Point> points)
        {
            var M = this.Skewsymmat(l);
            var B = M.Turn().Multiply(A).Multiply(M);
            var lAbs = l.Abs();
            var result = new Matrix<double>(3, 2);

            if (lAbs.AnyOf(v => v > Constants.SMALL))
            {
                var i = lAbs.MaxIndex();
                var li = Vector<int>.RegSpace(0, 2).ShedIndex(i);

                var alpha = Math.Sqrt(B.SubMatrix(li, li).SymmatL().Det() * -1) / l[i];

                var C = B.Addition(M.Multiply(alpha));

                var cAbs = C.Abs();

                if (cAbs.AnyOf(v => v > Constants.SMALL))
                {
                    var ind = cAbs.MaxIndex();
                    var i0 = ind[0];
                    var i1 = ind[1];

                    var p0 = C.RowByIndex(i0).Divide(C[i0, 2]);
                    var p1 = C.ColumnByIndex(i1).Divide(C[2, i1]);

                    points.Add(new Point(p0[0], p0[1]));
                    points.Add(new Point(p1[0], p1[1]));
                }
            }
        }

        private Vector<Point> Intersect(Conic conicA, Conic conicB)
        {
            var result = new Vector<Point>();

            var A = conicA.Matrix;
            var B = conicB.Matrix;

            var alpha = A.Det();
            var beta =
                A.JoinRows(B, 2).Det() +
                A.JoinRows(B, 1).Det() +
                A.JoinRows(B, 0).Det();
            var gamma =
                A.JoinRows(B, 1, 2).Det() +
                A.JoinRows(B, 0, 2).Det() +
                A.JoinRows(B, 0, 1).Det();
            var delta = B.Det();

            var roots = this.SolveCubic(alpha, beta, gamma, delta);

            var lambda = 0.0d;
            foreach (var root in roots)
                if (NearlyEquals(root.Imaginary, 0.0d))
                {
                    if (Math.Abs(root.Real) > lambda)
                        lambda = root.Real;
                }

            var C = A.Multiply(lambda).Addition(B);

            C.ForEach((m, i, j) =>
            {
                if (Math.Abs(m[i, j]) < Constants.SMALL)
                    m[i, j] = 0.0d;
            });

            var lines = this.SplitConic(C);

            if (lines.IsFinite())
            {
                this.IntersectConicLine(A, lines.ColumnByIndex(0), result);
                this.IntersectConicLine(A, lines.ColumnByIndex(1), result);
            }

            return result;
        }

        private Vector<Vector<int>> SetIndex(int n)
        {
            var result = new Vector<Vector<int>>();

            for (var i = 0; i < n; i++)
            {
                var v = new Vector<bool>(n);
                v.Fill(0, i, true);
                var permutations = v.GetPermutations();
                foreach (var permutation in permutations)
                {
                    var ind = new Vector<int>();

                    for (var j = 0; j < n; j++)
                        if (permutation[j])
                            ind.Add(j);

                    result.Add(ind);
                }
            }

            return result;
        }

        public double[] SourceParameters { get; private set; }

        public double[] SourceAreas { get; private set; }

        public double[] TargetParameters { get; private set; }

        public TimeSpan DurationTime { get; private set; }
    }
    #endregion
}
