﻿using System;
namespace Eulerr.API.Domain.Classes.Eulerr
{
    #region Constants
    internal static class Constants
    {
        public static readonly double SMALL = Math.Pow(Double.Epsilon, 0.95d);

        public static readonly double INF = Double.PositiveInfinity;
    }
    #endregion
}
