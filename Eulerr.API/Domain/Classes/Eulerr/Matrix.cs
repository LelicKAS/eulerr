﻿using System;
using System.Numerics;
using Eulerr.API.Domain.Classes.Eulerr.Calculators;

namespace Eulerr.API.Domain.Classes.Eulerr
{
    #region Matrix
    internal class Matrix<T> : ICloneable
    {
        #region Static members
        public static Matrix<Complex> DoubleToComplex(Matrix<double> matrix)
        {
            var result = new Matrix<Complex>(matrix.Rows, matrix.Cols);
            result.ForEach((m, i, j) => {
                m[i, j] = matrix[i, j];
            });
            return result;
        }
        #endregion

        private T[,] _matrix;
        private ICalculator<T> _calculator = null;

        public Matrix(int rows, int cols)
        {
            this.Cols = cols;
            this.Rows = rows;

            _matrix = new T[rows, cols];

            _calculator = Calculator<T>.GetCalculator<T>();

            this.Clear();
        }

        public Matrix<T> JoinRows(Matrix<T> matrix, params int[] columns)
        {
            var result = (this.Clone() as Matrix<T>);
            foreach (var column in columns)
                for (var i = 0; i < result.Rows; i++)
                    result[i, column] = matrix[i, column];
            return result;
        }

        public Matrix<T> JoinColumns(Matrix<T> matrix, params int[] rows)
        {
            var result = (this.Clone() as Matrix<T>);
            foreach (var row in rows)
                for (var j = 0; j < result.Cols; j++)
                    result[row, j] = matrix[row, j];
            return result;
        }

        public Vector<T> ToVector()
        {
            var result = new Vector<T>();
            this.ForEach((m, i, j) => {
                result.Add(m[i, j]);
            });
            return result;
        }

        public Matrix<T> Abs()
        {
            var result = (this.Clone() as Matrix<T>);
            result.ForEach((m, i, j) => {
                m[i, j] = _calculator.Abs(m[i, j]);
            });
            return result;
        }

        public Matrix<T> Multiply(T a)
        {
            var result = (this.Clone() as Matrix<T>);
            result.ForEach((m, i, j) => {
                m[i, j] = _calculator.Multiply(m[i, j], a);
            });
            return result;
        }

        public Matrix<T> Multiply(Matrix<T> matrix)
        {
            var result = (this.Clone() as Matrix<T>);
            result.ForEach((m, i, j) => {
                var v = m.DefaultValue;
                for (int k = 0; k < this.Rows; k++)
                    v = _calculator.Addition(v, _calculator.Multiply(this[i, k], matrix[k, j]));
                m[i, j] = v;
            });
            return result;
        }

        public bool AnyOf(Func<T, bool> func)
        {
            var result = true;
            var i = 0;
            while ((i < this.Rows) && (result))
            {
                var j = 0;
                while ((j < this.Cols) && (result))
                {
                    result = func(this[i, j]);
                    j++;
                }
                i++;
            }
            return result;
        }

        public Matrix<T> Addition(T a)
        {
            var result = (this.Clone() as Matrix<T>);
            result.ForEach((m, i, j) => {
                m[i, j] = _calculator.Addition(m[i, j], a);
            });
            return result;
        }

        public T Max()
        {
            var max = this[0, 0];
            this.ForEach((m, i, j) => {
                if (_calculator.Compare(m[i, j], max) > 0)
                    max = m[i, j];
            });
            return max;
        }

        public Vector<int> MaxIndex()
        {
            var max = this[0, 0];
            var maxI = 0;
            var maxJ = 0;
            this.ForEach((m, i, j) => {
                if (_calculator.Compare(m[i, j], max) > 0)
                {
                    max = m[i, j];
                    maxI = i;
                    maxJ = j;
                }
            });
            var result = new Vector<int>(2);
            result[0] = maxI;
            result[1] = maxJ;
            return result;
        }

        public T Min()
        {
            var min = this[0, 0];
            this.ForEach((m, i, j) => {
                if (_calculator.Compare(m[i, j], min) < 0)
                    min = m[i, j];
            });
            return min;
        }

        public Vector<int> MinIndex()
        {
            var min = this[0, 0];
            var minI = 0;
            var minJ = 0;
            this.ForEach((m, i, j) => {
                if (_calculator.Compare(m[i, j], min) < 0)
                {
                    min = m[i, j];
                    minI = i;
                    minJ = j;
                }
            });
            var result = new Vector<int>(2);
            result[0] = minI;
            result[1] = minJ;
            return result;
        }

        public Matrix<T> Addition(Matrix<T> a)
        {
            var result = (this.Clone() as Matrix<T>);
            result.ForEach((m, i, j) => {
                m[i, j] = _calculator.Addition(m[i, j], a[i, j]);
            });
            return result;
        }

        public Matrix<T> SymmatL()
        {
            var result = (this.Clone() as Matrix<T>);
            result.ForEach((m, i, j) => {
                if (i > j)
                    m[j, i] = m[i, j];
            });
            return result;
        }

        public Matrix<T> SymmatU()
        {
            var result = (this.Clone() as Matrix<T>);
            result.ForEach((m, i, j) => {
                if (i < j)
                    m[j, i] = m[i, j];
            });
            return result;
        }

        public T Det()
        {
            return _calculator.Det(this);
        }

        public void Fill(Action<Matrix<T>, int, int> action)
        {
            for (var i = 0; i < this.Rows; i++)
                for (var j = 0; j < this.Cols; j++)
                    action(this, i, j);
        }

        public Matrix<T> SubMatrix(Vector<int> rows, Vector<int> cols)
        {
            var result = new Matrix<T>(rows.Size, cols.Size);
            result.ForEach((m, i, j) => m[i, j] = this[rows[i], cols[j]]);
            return result;
        }

        public Matrix<T> SubMatrix(int i, int j)
        {
            var result = new Matrix<T>(this.Rows - 1, this.Cols - 1);
            for (var l = 0; l < this.Rows; l++)
            {
                for (var c = 0; c < this.Cols; c++)
                {
                    if ((l != i) && (c != j))
                    {
                        var newRow = (l < i ? l : l - 1);
                        var newCol = (c < j ? c : c - 1);

                        result[newRow, newCol] = this[l, c];
                    }
                }
            }
            return result;
        }

        public override string ToString()
        {
            var result = Environment.NewLine;
            for (var i = 0; i < this.Rows; i++)
            {
                for (var j = 0; j < this.Cols; j++)
                    result += this[i, j] + " ";
                result += Environment.NewLine;
            }
            return result;
        }

        public void Clear()
        {
            for (var i = 0; i < this.Rows; i++)
                for (var j = 0; j < this.Cols; j++)
                    _matrix[i, j] = this.DefaultValue;
        }

        public void ForEach(Action<Matrix<T>, int, int> action)
        {
            for (var i = 0; i < this.Rows; i++)
                for (var j = 0; j < this.Cols; j++)
                    action(this, i, j);
        }

        public Matrix<T> Turn()
        {
            var result = (this.Clone() as Matrix<T>);
            result.ForEach((m, i, j) => m[i, j] = this[j, i]);
            return result;
        }

        public bool IsFinite()
        {
            var result = true;
            var i = 0;
            while ((i < this.Rows) && (result))
            {
                var j = 0;
                while ((j < this.Cols) && (result))
                {
                    result = _calculator.IsFinite(this[i, j]);
                    j++;
                }
                i++;
            }
            return result;
        }

        public Vector<T> RowByIndex(int i)
        {
            var vector = new Vector<T>();
            for (var j = 0; j < this.Cols; j++)
                vector.Add(this[i, j]);
            return vector;
        }

        public Vector<T> ColumnByIndex(int j)
        {
            var vector = new Vector<T>();
            for (var i = 0; i < this.Rows; i++)
                vector.Add(this[i, j]);
            return vector;
        }

        public Vector<T> Diagonal()
        {
            var result = new Vector<T>();
            this.ForEach((m, i, j) => {
                if (i == j)
                    result.Add(m[i, j]);
            });
            return result;
        }

        #region Clone
        public object Clone()
        {
            var copy = new Matrix<T>(this.Rows, this.Cols);
            for (var i = 0; i < this.Rows; i++)
                for (var j = 0; j < this.Cols; j++)
                    copy[i, j] = this[i, j];
            return copy;
        }
        #endregion

        public T this[int i, int j]
        {
            get => _matrix[i, j];
            set => _matrix[i, j] = value;
        }

        public T DefaultValue => default(T);

        public int Cols { get; private set; }

        public int Rows { get; private set; }
    }
    #endregion
}
