﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Eulerr.API.Domain.Classes.Eulerr.Calculators;

namespace Eulerr.API.Domain.Classes.Eulerr
{
    #region Vector
    internal class Vector<T> : ICloneable, IEnumerable<T>
    {
        #region Static members
        public static Vector<int> SetIntersection(Vector<int> vector1, Vector<int> vector2)
        {
            var v1 = (vector1.Clone() as Vector<int>);
            var v2 = (vector2.Clone() as Vector<int>);

            v1.Sort(delegate (int i, int j) {
                return i.CompareTo(j);
            });

            v2.Sort(delegate (int i, int j) {
                return i.CompareTo(j);
            });

            var result = new Vector<int>();

            foreach (var i in v1)
                if (v2.IndexOf(i) >= 0)
                    result.Add(i);

            return result;
        }

        public static Vector<Complex> DoubleToComplex(Vector<double> vector)
        {
            var result = new Vector<Complex>();
            vector.ForEach((v, i) => {
                result.Add(v[i]);
            });
            return result;
        }

        public static Vector<int> RegSpace(int start, int end, int step = 1)
        {
            var result = new Vector<int>();
            var value = start;
            while (value <= end)
            {
                result.Add(value);
                value += step;
            }
            return result;
        }
        #endregion

        private List<T> _vector = new List<T>();

        private ICalculator<T> _calculator = null;

        public Vector()
        {
            _calculator = Calculator<T>.GetCalculator<T>();
        }

        public Vector(int size) : this()
        {
            for (var i = 0; i < size; i++)
                this.Add(this.DefaultValue);
        }

        public Vector(params T[] values) : this()
        {
            foreach (var value in values)
                this.Add(value);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return ((obj is Vector<T>) && ((obj as Vector<T>).ToString().Equals(this.ToString())));
        }

        public override string ToString()
        {
            var values = _vector
                .Select(v => v.ToString())
                .ToArray();
            return String.Join(" ", values);
        }

        public T[] ToArray()
        {
            return _vector.ToArray();
        }

        public void Add(T e)
        {
            _vector.Add(e);
        }

        public void Fill(int startIndex, int finishIndex, T value)
        {
            for (var index = startIndex; index <= finishIndex; index++)
                this[index] = value;
        }

        public void Fill(int start, int finish, Func<int, T> func)
        {
            for (var index = start; index <= finish; index++)
                this[index] = func(index);
        }

        public void Fill(Func<int, T> func)
        {
            this.Fill(0, this.Size - 1, func);
        }

        public T Accumulate(int start, int finish, T init = default(T))
        {
            T result = init;
            for (int index = start; index <= finish; index++)
                result = _calculator.Addition(result, this[index]);
            return result;
        }

        public T Accumulate(T init = default(T))
        {
            return this.Accumulate(0, this.Size - 1, init);
        }

        public void Transform(int start, int finish, int updateStart, Func<T, T> func)
        {
            var updateIndex = updateStart;
            for (int index = start; index <= finish; index++)
            {
                this[updateIndex] = func(this[index]);
                updateIndex++;
            }
        }

        public void Transform(int start, int finish, Func<T, T> func)
        {
            this.Transform(start, finish, start, func);
        }

        public void Transform(Func<T, T> func)
        {
            this.Transform(0, this.Size - 1, func);
        }

        public void Clear()
        {
            _vector.Clear();
        }

        public void ForEach(Action<Vector<T>, int> action)
        {
            for (var i = 0; i < this.Size; i++)
                action(this, i);
        }

        public Vector<T> Abs()
        {
            var result = (Vector<T>)this.Clone();
            result.ForEach((v, i) => {
                v[i] = _calculator.Abs(v[i]);
            });
            return result;
        }

        public T InnerProduct(Vector<T> bVector, int start, int finish, int bStart, T init, Func<T, T, T> func)
        {
            T result = init;
            var bIndex = bStart;
            for (var index = start; index <= finish; index++)
            {
                result = _calculator.Addition(result, func(this[index], bVector[bIndex]));
                bIndex++;
            }
            return result;
        }

        public T InnerProduct(Vector<T> bVector, int start, int finish, T init, Func<T, T, T> func)
        {
            return this.InnerProduct(bVector, start, finish, start, init, func);
        }

        public T InnerProduct(Vector<T> bVector, T init, Func<T, T, T> func)
        {
            return this.InnerProduct(bVector, 0, this.Size - 1, init, func);
        }

        public T InnerProduct(Vector<T> bVector, Func<T, T, T> func)
        {
            return this.InnerProduct(bVector, this.DefaultValue, func);
        }

        public Vector<T> Divide(T a)
        {
            var result = (Vector<T>)this.Clone();
            result.ForEach((v, i) => {
                v[i] = _calculator.Divide(v[i], a);
            });
            return result;
        }

        public void Sort(Comparison<T> comparison)
        {
            _vector.Sort(comparison);
        }

        public Vector<T> ShedIndex(int index)
        {
            var result = new Vector<T>();
            this.ForEach((v, i) => {
                if (i != index)
                    result.Add(v[i]);
            });
            return result;
        }

        public T Min()
        {
            T min = _vector[0];
            for (var index = 1; index < _vector.Count; index++)
                if (_calculator.Compare(_vector[index], min) < 0)
                    min = _vector[index];
            return min;
        }

        public int MinIndex()
        {
            return this.IndexOf(this.Min());
        }

        public T Max()
        {
            T max = _vector[0];
            for (var index = 1; index < _vector.Count; index++)
                if (_calculator.Compare(_vector[index], max) > 0)
                    max = _vector[index];
            return max;
        }

        public int MaxIndex()
        {
            return this.IndexOf(this.Max());
        }

        public bool AnyOf(int start, int finish, Func<T, bool> func)
        {
            var result = false;
            var i = start;
            while ((i <= finish) && (!result))
            {
                result = func(this[i]);
                i++;
            }
            return result;
        }

        public bool AnyOf(Func<T, bool> func)
        {
            return AnyOf(0, this.Size - 1, func);
        }

        public bool AllOf(int start, int finish, Func<T, bool> func)
        {
            var result = true;
            var i = start;
            while ((i <= finish) && (result))
            {
                result = func(this[i]);
                i++;
            }
            return result;
        }

        public bool AllOf(Func<T, bool> func)
        {
            return this.AllOf(0, this.Size - 1, func);
        }

        public bool IsFinite()
        {
            var result = true;
            var i = 0;
            while ((i < this.Size) && (result))
            {
                result = _calculator.IsFinite(this[i]);
                i++;
            }
            return result;
        }

        public int IndexOf(T e)
        {
            return _vector.IndexOf(e);
        }

        public Vector<Vector<T>> GetPermutations()
        {
            var result = new Vector<Vector<T>>();
            var copy = this.Clone();
            result.Add(copy);
            this.Permutation(result, copy, 0, copy.Size - 1);
            return result;
        }

        private void Permutation(Vector<Vector<T>> result, Vector<T> source, int start, int end)
        {
            if (start != end)
            {
                for (var i = start; i <= end; i++)
                {
                    var copy = source.Clone();
                    if (!copy[start].Equals(copy[i]))
                    {
                        copy.Swap(start, i);
                        if (result.IndexOf(copy) < 0)
                            result.Add(copy);
                    }
                    this.Permutation(result, copy, start + 1, end);
                }
            }
        }

        public Vector<T> Clone()
        {
            var result = new Vector<T>();
            foreach (var value in this)
                result.Add(value);
            return result;
        }

        public void Swap(int first, int second)
        {
            var temp = this[first];
            this[first] = this[second];
            this[second] = temp;
        }

        public T this[int index]
        {
            get => _vector[index];
            set => _vector[index] = value;
        }

        public int Size => _vector.Count;

        public T DefaultValue => default(T);

        #region IEnumerable<T>
        public IEnumerator<T> GetEnumerator()
        {
            return _vector.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _vector.GetEnumerator();
        }
        #endregion

        #region IClonable
        object ICloneable.Clone() => this.Clone();
        #endregion
    }
    #endregion
}
