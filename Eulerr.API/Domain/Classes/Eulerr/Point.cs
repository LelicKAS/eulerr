﻿using System;
namespace Eulerr.API.Domain.Classes.Eulerr
{
    #region Point
    public class Point : IComparable<Point>
    {
        public Point()
        {
        }

        public Point(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public Point Clone()
        {
            return new Point(this.X, this.Y);
        }

        public void Rotate(double angle)
        {
            var x = this.X;
            var y = this.Y;

            this.X = x * Math.Cos(angle) - y * Math.Sin(angle);
            this.Y = x * Math.Sin(angle) + y * Math.Cos(angle);
        }

        public void Rotate(double angle, Point center)
        {
            this.Translate(-center.X, -center.Y);
            this.Rotate(angle);
            this.Translate(center.X, center.Y);
        }

        public void Translate(double x, double y)
        {
            this.X += x;
            this.Y += y;
        }

        public void Scale(double xScale, double yScale)
        {
            this.X *= xScale;
            this.Y *= yScale;
        }

        #region IComporable<Point>
        public int CompareTo(Point other)
        {
            return 0;
        }
        #endregion

        public double X { get; set; } = 0.0d;

        public double Y { get; set; } = 0.0d;
    }
    #endregion
}
