﻿using System;
using System.Collections.Generic;

namespace Eulerr.API.Domain.Classes.Eulerr
{
    #region NLMProcess
    internal class NLMProcess
    {
        private List<Iterator> _iterators = new List<Iterator>();

        public NLMProcess(IList<Iterator> iterators)
        {
            this.MinValue = double.MaxValue;
            if (iterators != null)
                _iterators.AddRange(iterators);
        }

        public void AddIterator(Iterator iterator)
        {
            _iterators.Add(iterator);
        }

        public void Start(Vector<double> pars, Func<Vector<double>, double> func, Action minValueChanged)
        {
            if (_iterators.Count > 0)
                this.Iterate(pars, func, minValueChanged, 0);
        }

        private void Iterate(Vector<double> pars, Func<Vector<double>, double> func, Action minValueChanged, int index)
        {
            var iterator = _iterators[index];
            foreach (var v in iterator.Values)
            {
                pars[iterator.Index] = v;
                var value = func(pars);
                if (value < this.MinValue)
                {
                    this.MinValue = value;
                    this.MinPars = pars.Clone();
                    minValueChanged();
                }
                if (index < _iterators.Count - 1)
                    this.Iterate(pars, func, minValueChanged, index + 1);
            }
        }

        public Vector<double> MinPars { get; private set; }

        public double MinValue { get; private set; }
    }
    #endregion
}
