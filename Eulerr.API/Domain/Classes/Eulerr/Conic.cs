﻿using System;

namespace Eulerr.API.Domain.Classes.Eulerr
{
    #region Conic
    internal class Conic
    {
        public Conic(Ellipse ellipse)
        {
            var x = ellipse.X;
            var y = ellipse.Y;
            var a = ellipse.A;
            var b = ellipse.B;
            var angle = ellipse.Angle;
            var sin = Math.Sin(angle);
            var cos = Math.Cos(angle);

            var A = a * a * sin * sin + b * b * cos * cos;
            var B = 2.0d * (b * b - a * a) * sin * cos;
            var C = a * a * cos * cos + b * b * sin * sin;
            var D = -2.0d * A * x - B * y;
            var E = -B * x - 2.0d * C * y;
            var F = A * x * x - B * x * y + C * y * y - a * a * b * b;

            B *= 0.5d;
            D *= 0.5d;
            E *= 0.5d;

            this.Matrix = new Matrix<double>(3, 3);

            this.Matrix[0, 0] = A;
            this.Matrix[0, 1] = B;
            this.Matrix[0, 2] = D;
            this.Matrix[1, 0] = B;
            this.Matrix[1, 1] = C;
            this.Matrix[1, 2] = E;
            this.Matrix[2, 0] = D;
            this.Matrix[2, 1] = E;
            this.Matrix[2, 2] = F;

            this.Matrix.ForEach((matrix, i, j) => {
                if (Math.Abs(matrix[i, j]) < Constants.SMALL)
                    matrix[i, j] = matrix.DefaultValue;
            });
        }

        public override string ToString()
        {
            return this.Matrix.ToString();
        }

        public Matrix<double> Matrix { get; private set; }
    }
    #endregion
}
