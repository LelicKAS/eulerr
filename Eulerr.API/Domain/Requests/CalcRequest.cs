﻿using System;
using System.Text.Json.Serialization;

namespace Eulerr.API.Domain.Requests
{
    #region CalcRequest
    /// <summary>
    /// Request Data Model for Calculation
    /// </summary>
    public class CalcRequest
    {
        /// <summary>
        /// Array of sizes of intersections of circles
        /// </summary>
        [JsonPropertyName("areas")]
        public double[] Areas { get; set; }
    }
    #endregion
}
