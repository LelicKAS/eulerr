﻿using System;
using System.Text.Json.Serialization;

namespace Eulerr.API.Domain.Requests
{
    #region OptimizeRequest
    /// <summary>
    /// Request Data Model for Optimization
    /// </summary>
    public class OptimizeRequest
    {
        /// <summary>
        /// Initial circle data array
        /// </summary>
        [JsonPropertyName("parameters")]
        public double[] Parameters { get; set; }

        /// <summary>
        /// Array of sizes of intersections of circles
        /// </summary>
        [JsonPropertyName("areas")]
        public double[] Areas { get; set; }
    }
    #endregion
}
