﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eulerr.API.Domain.Classes.Eulerr;
using Eulerr.API.Domain.Requests;
using Eulerr.API.Domain.Responses;
using Eulerr.API.Domain.Services;
using Microsoft.Extensions.Logging;

namespace Eulerr.API.Services
{
    #region EullerService
    /// <summary>
    /// Service for Optimization Circles (Venn Diagram)
    /// </summary>
    public class EulerrService : IEulerrService
    {
        public ILogger<EulerrService> _logger = null;

        public EulerrService(ILogger<EulerrService> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Calculate Circles
        /// </summary>
        /// <param name="request">Data for calculation</param>
        /// <returns>Optimize data</returns>
        public async Task<OptimizeResponse> Calc(CalcRequest request)
        {
            var areas = Optimization.UnionAreas(request.Areas);

            var circles = this.InitializeCircles(areas);
            var intersections = this.InitializeIntersections(circles, areas);

            if (circles.Length > 2)
                this.CheckCenters(intersections);

            this.CalcCenters(intersections);

            var parsList = new List<double>();
            foreach (var circle in circles)
            {
                parsList.Add(circle.X);
                parsList.Add(circle.Y);
                parsList.Add(circle.A);
            };

            var parameters = parsList.ToArray();

            var response = new OptimizeResponse {
                Parameters = parameters
            };

            var circlesCount = circles.Where(c => c.Size > 0).Count();

            if (circlesCount > 2)
            {
                var optimizeRequest = new OptimizeRequest {
                    Areas = request.Areas,
                    Parameters = parameters
                };
                var optimizeResponse = await this.Optimize(optimizeRequest);
                response.Parameters = optimizeResponse.Parameters;
            }

            return response;
        }

        /// <summary>
        /// Optimize Circles
        /// </summary>
        /// <param name="request">Data for optimization</param>
        /// <returns>Optimize data</returns>
        public async Task<OptimizeResponse> Optimize(OptimizeRequest request)
        {
            var optimization = new Optimization(request.Parameters, request.Areas);
            await optimization.Optimize();

            _logger.LogInformation($"{optimization}");

            return new OptimizeResponse {
                Parameters = optimization.TargetParameters
            };
        }

        private Ellipse[] InitializeCircles(double[] areas)
        {
            var circles = new List<Ellipse>();
            if (areas.Length > 0)
                circles.Add(this.CreateCircle(areas[0]));
            if (areas.Length > 2)
                circles.Add(this.CreateCircle(areas[1]));
            if (areas.Length > 6)
                circles.Add(this.CreateCircle(areas[2]));
            return circles.ToArray();
        }

        private Intersection[] InitializeIntersections(Ellipse[] ellipses, double[] areas)
        {
            var intersections = new List<Intersection>();
            if (ellipses.Length > 1)
            {
                var size = (ellipses.Length == 2 ? areas[2] : areas[3]);
                intersections.Add(this.CreateIntersection(ellipses[0], ellipses[1], size));
            }
            if (ellipses.Length > 2)
            {
                intersections.Add(this.CreateIntersection(ellipses[0], ellipses[2], areas[4]));
                intersections.Add(this.CreateIntersection(ellipses[1], ellipses[2], areas[5]));
            }
            return intersections.ToArray();
        }

        private Ellipse CreateCircle(double size)
        {
            var circle = new Ellipse(size);
            return circle;
        }

        private Intersection CreateIntersection(Ellipse firstCircle, Ellipse secondCircle, double size)
        {
            var intersection = new Intersection(firstCircle, secondCircle, size);
            return intersection;
        }

        private void CheckCenters(Intersection[] intersections)
        {
            //var minIndex = -1;
            //var maxIndex = -1;
            //var middleIndex = -1;
            //var minDistance = Double.MaxValue;
            //var maxDistance = Double.MinValue;
            //var middleDistance = 0.0d;

            //for (int index = 0; index < intersections.Length; index++)
            //{
            //    var distance = intersections[index].Distanсe;
            //    if (minDistance > distance)
            //    {
            //        minIndex = index;
            //        minDistance = distance;
            //    }
            //    if (maxDistance < distance)
            //    {
            //        maxIndex = index;
            //        maxDistance = distance;
            //    }
            //}

            //var i = 0;
            //while ((i < intersections.Length) && (middleIndex < 0))
            //{
            //    if ((i != minIndex) && (i != maxIndex))
            //    {
            //        middleIndex = i;
            //        middleDistance = intersections[i].Distanсe;
            //    }
            //    i++;
            //}

            //if (maxDistance > minDistance + middleDistance)
            //    intersections[middleIndex].Distanсe += maxDistance - minDistance + 0.1d;
        }

        private void CalcCenters(Intersection[] intersections)
        {
            if (intersections.Length > 0)
            {
                var a = intersections[0].Distanсe;

                intersections[0].SecondCircle.X = intersections[0].Distanсe;

                if (intersections.Length > 2)
                {
                    var b = intersections[1].Distanсe;
                    var c = intersections[2].Distanсe;
                    var cosA = (a * a + b * b - c * c) / (2 * a * b);
                    var cosB = (a * a + c * c - b * b) / (2 * a * c);
                    var cosC = (b * b + c * c - a * a) / (2 * b * c);


                    if ((Math.Abs(cosA) <= 1.0d) &&
                        (Math.Abs(cosB) <= 1.0d) &&
                        (Math.Abs(cosC) <= 1.0d))
                    {
                        var firstAngle = Math.Acos(Math.Abs(cosA));
                        var secondAngle = Math.Acos(Math.Abs(cosB));
                        var thirdAngle = Math.Acos(Math.Abs(cosC));

                        if (cosA < 0)
                            firstAngle = Math.PI - firstAngle;
                        if (cosB < 0)
                            secondAngle = Math.PI - secondAngle;
                        if (cosC < 0)
                            thirdAngle = Math.PI - thirdAngle;

                        var firstX = intersections[1].Distanсe * Math.Cos(firstAngle);
                        var firstY = intersections[1].Distanсe * Math.Sin(firstAngle);

                        var secondX = intersections[2].Distanсe * Math.Cos(Math.PI - secondAngle) + intersections[0].Distanсe;
                        var secondY = intersections[2].Distanсe * Math.Sin(Math.PI - secondAngle);

                        if ((Math.Abs(firstX - secondX) > 0.0001) ||
                            (Math.Abs(firstY - secondY) > 0.0001))
                            throw new Exception("An error in calculating the centers of the circles.");

                        intersections[2].SecondCircle.X = firstX;
                        intersections[2].SecondCircle.Y = firstY * -1;
                    }
                    else
                        intersections[2].SecondCircle.X = intersections[2].Distanсe;
                }
            }
        }

        private bool CheckFit(Vector<double> areas, Vector<double> fit)
        {
            var result = true;
            var index = 0;
            while ((index < areas.Size) && (result))
            {
                result = (((areas[index] > 0) && (fit[index] > 0)) || ((areas[index] == 0) && (fit[index] == 0)));
                index++;
            }
            return result;
        }
    }
    #endregion
}
